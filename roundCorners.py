#!/usr/bin/env python3
"""
Script to look for graphviz-style polyline paths in an SVG file (M
followed by C sequence for the path data) and smooth out their corners.

Use:

```
  graph [splines=polyline]
```

In your `.dot` file to set things up correctly.
"""

STRAIGHT_FRACTION = 0.6
"""
What fraction of each line should remain straight.
"""

MAX_CURVE_PART = 70
"""
Maximum length in user units that we'll turn into a curve, overriding
STRAIGHT_FRACTION if STRAIGHT_FRACTION would generate a longer curved
part. Set to `None` to disable the cap.
"""

import re
import sys

try:
    import svg.path
except ImportError:
    print("Did you forget to `pip install svg.path`?")
    exit(1)

def imagDist(a, b):
    """
    Computes distance between two imaginary numbers.
    """
    return ((a.real - b.real)**2 + (a.imag - b.imag)**2)**0.5

def scaledTo(v, lgth):
    """
    Returns the given vector (a complex number) scaled to the given
    length.

    Zero-length vectors are returned unchanged.
    """
    scale = imagDist(0, v)
    if scale == 0:
        return v
    unit = v / scale
    return lgth * unit

def smootherPath(match):
    """
    Given an SVG path data string consisting of a starting 'M' followed
    by a single 'C' with one or more stops, returns a new data string
    for a path that moves towards the same inflection points but which
    turns smoothly past them instead of hitting them exactly. We ignore
    the original curvature of the line and instead draw straight lines
    for STRAIGHT_FRACTION of the way between the points, and then use a
    quadratic Bezier curve with the target point as a control point to
    smoothly join the ends of the truncated lines.
    """
    fractionFromEnd = (1 - STRAIGHT_FRACTION) / 2
    group = match[1]
    orig = svg.path.parse_path(group)
    result = svg.path.Path(orig[0])  # first command is M
    targets = []
    for i in range(1, len(orig)):
        prev = orig[i - 1]
        this = orig[i]
        prevLength = imagDist(prev.start, prev.end)
        prevVector = prev.end - prev.start
        prevSmooth = min(
            MAX_CURVE_PART,
            prevLength * (1 - STRAIGHT_FRACTION) / 2
        )
        prevStop = prev.start + scaledTo(prevVector, prevLength - prevSmooth)
        segLength = imagDist(this.start, this.end)
        smoothPart = min(
            MAX_CURVE_PART,
            segLength * (1 - STRAIGHT_FRACTION) / 2
        )
        thisVector = this.end - this.start
        thisStart = this.start + scaledTo(thisVector, smoothPart)
        thisStop = this.start + scaledTo(thisVector, segLength - smoothPart)
        result.append(
            svg.path.QuadraticBezier(prevStop, this.start, thisStart)
        )
        result.append(svg.path.Line(thisStart, thisStop))

    # Fix up first segment after initial M
    result.pop(1)
    result[1].start = result[0].end
    # Fix up last segment
    result[-1].end = this.end  # leftover var from loop

    return f'd="{result.d()}"'

def main():
    """
    Reads from stdin and writes updated SVG to stdout.

    Currently NOT a streaming algorithm so it reads all of stdin before
    writing stdout in one block.
    """
    orig = sys.stdin.read()
    # TODO: Bother to find actual path nodes?
    print(re.sub(r'd="(M[0-9][^"]*)"', smootherPath, orig))

if __name__ == "__main__":
    main()
